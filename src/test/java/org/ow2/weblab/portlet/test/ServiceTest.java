/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.test;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.portlet.business.bean.DisplayMetaConfBean;
import org.ow2.weblab.portlet.business.bean.EditMetaConfBean;
import org.ow2.weblab.portlet.business.bean.RDFPropertiesConfBean;
import org.ow2.weblab.portlet.business.bean.ServiceDescription;
import org.ow2.weblab.portlet.business.bean.TimeConfig;
import org.ow2.weblab.portlet.business.bean.config.map.GroupLayerConfig;
import org.ow2.weblab.portlet.business.bean.config.map.WMSLayerConfig;
import org.ow2.weblab.portlet.business.bean.taglib.DataTaglib;
import org.ow2.weblab.portlet.business.service.impl.WSBusinessServicesImpl;
import org.springframework.context.support.FileSystemXmlApplicationContext;


/**
 * @author ymombrun
 */
public class ServiceTest {


	private static WSBusinessServicesImpl SERVICE;


	@BeforeClass
	public static void init() {

		final FileSystemXmlApplicationContext fsxac = new FileSystemXmlApplicationContext("src/main/webapp/WEB-INF/weblabmetaview-portlet.xml");
		ServiceTest.SERVICE = fsxac.getBean("metaviewBusinessService", WSBusinessServicesImpl.class);
	}


	@Test
	public void testLinkedToQuery() throws Exception {

		Assert.assertNotNull(ServiceTest.SERVICE);
		final PieceOfKnowledge pok = WebLabResourceFactory.createResource("ServiceTest", UUID.randomUUID().toString(), PieceOfKnowledge.class);
		final WRetrievalAnnotator wra = new WRetrievalAnnotator(URI.create(pok.getUri()), pok);
		final String uriInput = "weblab://yataaaa";
		wra.writeLinkedTo(URI.create(uriInput));
		final String uriResult = ServiceTest.SERVICE.getURIFromPoK(pok);
		Assert.assertEquals(uriInput, uriResult);
	}


	@Test
	public void testSearchTranslations() throws Exception {

		ServiceTest.SERVICE.getTranslations(new Document(), Locale.FRANCE);
	}


	@Test
	public void testUselessButCoverage() throws Exception {

		final DataTaglib dtl = new DataTaglib();
		Assert.assertNull(dtl.getFormats());
		Assert.assertNull(dtl.getLanguages());

		final TimeConfig tc = new TimeConfig();
		Assert.assertNull(tc.getFormatDateTimeForJSTL());
		Assert.assertTrue(((tc.getMaxEndDateMS() - System.currentTimeMillis()) > -1000) && ((tc.getMaxEndDateMS() - System.currentTimeMillis()) <= 0));

		final EditMetaConfBean emcb = new EditMetaConfBean();
		Assert.assertNull(emcb.getProperties());
		Assert.assertNull(emcb.getLayersConfig());
		Assert.assertFalse(emcb.isUseGeoserverExposedMap());

		final DisplayMetaConfBean dmcb = new DisplayMetaConfBean();
		Assert.assertNotNull(dmcb.getProperties());
		Assert.assertFalse(dmcb.isUseGeoserverExposedMap());
		Assert.assertNotNull(dmcb.getLayersConfig());

		final ServiceDescription sd = new ServiceDescription();
		sd.setUri("uri");
		Assert.assertEquals("uri", sd.getUri());
		sd.setLabel("label");
		Assert.assertEquals("label", sd.getLabel());

		final WMSLayerConfig wmslc = new WMSLayerConfig();
		Assert.assertFalse(wmslc.isTiled());
		Assert.assertNull(wmslc.getRelativeUrl());
		wmslc.setUrl("url");
		Assert.assertEquals("url", wmslc.getUrl());
		wmslc.setLayer("layer");
		Assert.assertEquals("layer", wmslc.getLayer());

		final GroupLayerConfig glc1 = new GroupLayerConfig();
		glc1.setLayers(Collections.singletonList(wmslc));
		final GroupLayerConfig glc2 = new GroupLayerConfig(glc1);
		Assert.assertNotNull(glc2.getType());
		Assert.assertNotNull(glc2.toString());
		Assert.assertNotNull(glc2.getProjection());
		glc2.getLayers().add(glc1);
		final GroupLayerConfig glc3 = new GroupLayerConfig(glc2);
		Assert.assertEquals(2, glc3.getLayers().size());

		final RDFPropertiesConfBean rdfpcb = new RDFPropertiesConfBean();
		Assert.assertNull(rdfpcb.getProperties());
		rdfpcb.setProperties(new HashMap<>());
		Assert.assertNotNull(rdfpcb.getDistinctAnnotationsNS());
		Assert.assertNull(rdfpcb.getAnnotationValue(""));
	}

}
