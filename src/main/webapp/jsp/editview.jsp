<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://org.ow2.weblab/taglib" prefix="weblab"%>


<portlet:defineObjects />

<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="meta_portlet" />

<portlet:actionURL var="saveURL">
	<portlet:param name="action" value="save" />
</portlet:actionURL>

<portlet:actionURL var="cancelURL">
	<portlet:param name="action" value="cancel" />
</portlet:actionURL>

<c:url value="/images/delete.png" var="deleteURL"></c:url>
<fmt:message key="meta.edit.rate" var="rate" />
<fmt:message key="meta.edit.commented" var="commented" />
<fmt:message key="meta.edit.credibility" var="credibility" />
<fmt:message key="meta.edit.reliability" var="reliability" />
<fmt:message key="meta.edit.subject" var="subject" />
<fmt:message key="meta.edit.toKeep" var="toKeep" />
<fmt:message key="meta.label.toKeep" var="toKeepLabel" />
<fmt:message key="meta.edit.lon" var="lon" />
<fmt:message key="meta.edit.lat" var="lat" />
<fmt:message key="meta.label.location" var="docLocation" />
<fmt:message key="meta.edit.error.credibreliable" var="errorEmpty" />

<!-- <h1>Edition of the Meta Data</h1> -->
<div class="edit_meta_table">
	<form id="inputForm" class="inputForm" action="${saveURL}" method="post" onsubmit="return validateForm(this)">
		<table class="meta_data_table">
			<tbody>
				<c:if test="${metaDisplay['title'] && weblab:hasValue(resource, rdfProperties['title'])}">
					<tr>
						<c:set var="titleValue">
							<weblab:getPropertyValues uri="${rdfProperties['title']}" />
						</c:set>
						<th><fmt:message key="meta.title" /></th>
						<td><span class="error">${errorEmpty}</span> <input type="text" name="title" id="title" value='<c:out value="${titleValue}"></c:out>' class="textInput" required></td>
					</tr>
				</c:if>
				<c:if test="${metaDisplay['format'] && weblab:hasValue(resource, rdfProperties['format'])}">
					<tr>
						<th><fmt:message key="meta.format" /></th>
						<td><c:set var="formatValue">
								<weblab:getPropertyValues uri="${rdfProperties['format'] }" />
							</c:set>
								<input type="hidden" name="format" value="${formatValue}">
								<select onchange="changeSelectValue(this)">
									<option value="${formatValue}" selected>
										<weblab:renderValues style="text" cssClass="meta_data_formatIconText"><weblab:getPropertyValues uri="${rdfProperties['format'] }" /></weblab:renderValues></option>
								<c:forEach var="tagLibFormat" items="${dataTaglib}">
									<option value="${tagLibFormat.key}">${tagLibFormat.value}</option>
								</c:forEach>
						</select></td>
					</tr>
				</c:if>
				<c:if test="${metaDisplay['language'] && weblab:hasValue(resource, rdfProperties['language'])}">
					<tr>
						<th><fmt:message key="meta.language" /></th>
						<td><c:set var="langKey">
								<weblab:getPropertyValues uri="${rdfProperties['language'] }" />
							</c:set>
							<input type="hidden" name="language" value="${langKey}">
							<select onchange="changeSelectValue(this)">
								<c:forEach var="dataLang" items="${dataLanguage}">
									<c:choose>
										<c:when test="${langKey == dataLang.key}">
											<option value="${dataLang.key}" selected>${dataLang.value}</option>
										</c:when>
										<c:otherwise>
											<option value="${dataLang.key}">${dataLang.value}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
						</select></td>
					</tr>
				</c:if>
				<c:if test="${metaDisplay['created'] && weblab:hasValue(resource, rdfProperties['created'])}">
					<weblab:getPropertyValues uri="${rdfProperties['created'] }" var="createdDates" />
					<c:set var="createdDatesStr" value="" />
					<c:forEach items="${createdDates}" var="createdDate" varStatus="createdDateStatus">
						<c:if test="${createdDateStatus.count != 1}">
							<c:set var="createdDatesStr" value="${createdDatesStr}<br />" />
						</c:if>
						<c:catch var="createdDatesError">
							<fmt:formatDate value="${createdDate}" pattern="${timeConfig.formatDateTimeForJSTL}" var="createdDateStr" />
							<c:set var="createdDatesStr" value="${createdDatesStr}${createdDateStr}" />
						</c:catch>
					</c:forEach>
					<c:if test="${createdDatesError == null}">
						<tr>
							<th><fmt:message key="meta.created" /></th>
							<td><input name="created" type="date" class="datePicker" id="created" value="<c:out value="${createdDatesStr}" />" autocomplete="off" /></td>
						</tr>
					</c:if>
				</c:if>
				<c:if test="${metaDisplay['modified'] && weblab:hasValue(resource, rdfProperties['modified'])}">
					<weblab:getPropertyValues uri="${rdfProperties['modified'] }" var="modifiedDates" />
					<c:set var="modifiedDatesStr" value="" />
					<c:forEach items="${modifiedDates}" var="modifiedDate" varStatus="modifiedDateStatus">
						<c:if test="${modifiedDateStatus.count != 1}">
							<c:set var="modifiedDatesStr" value="${modifiedDatesStr}, " />
						</c:if>
						<c:catch var="modifiedDatesError">
							<fmt:formatDate value="${modifiedDate}" pattern="${timeConfig.formatDateTimeForJSTL}" var="modifiedDateStr" />
							<c:set var="modifiedDatesStr" value="${modifiedDatesStr}${modifiedDateStr}" />
						</c:catch>
					</c:forEach>
					<c:if test="${modifiedDatesError == null}">
						<tr>
							<th><fmt:message key="meta.modified" /></th>
							<td><input id="modified" type="date" name="modified" value="<c:out value="${modifiedDatesStr}" />" class="datePicker"></td>
						</tr>
					</c:if>
				</c:if>
				<c:if test="${metaDisplay['creator'] && weblab:hasValue(resource, rdfProperties['creator'])}">
					<tr>
						<th><fmt:message key="meta.creator" /></th>
						<td><input type="text" name="creator" value="<weblab:getPropertyValues
								uri="${rdfProperties['creator'] }" />" class="textInput"></td>
					</tr>
				</c:if>
				<c:if test="${metaDisplay['abstract'] && weblab:hasValue(resource, rdfProperties['abstract'])}">
					<tr>
						<th><fmt:message key="meta.abstract" /></th>
						<td><input type="text" name="abstract" value="<weblab:getPropertyValues
								uri="${rdfProperties['abstract'] }" />" class="textInput"></td>
					</tr>
				</c:if>
				<c:if test="${metaDisplay['description'] && weblab:hasValue(resource, rdfProperties['description'])}">
					<tr>
						<th><fmt:message key="meta.description" /></th>
						<td><input type="text" name="description" value="<weblab:getPropertyValues uri="${rdfProperties['description'] }" />" class="textInput"></td>
					</tr>
				</c:if>

				<c:choose>
					<c:when test="${metaDisplay['subject'] && weblab:hasValue(resource, rdfProperties['subject'])}">
						<tr>
							<th><fmt:message key="meta.subject" /></th>
							<td><input type="text" name="subject" class="textInput" placeholder="subjects value separated by commas"
								value="<weblab:getPropertyValues uri="${rdfProperties['subject'] }" var="subjs" /> <c:forEach var="subj" items="${subjs}" varStatus="status"><c:if test="${status.count != 1}">, </c:if>${subj}</c:forEach>"></td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr class="extraRow">
							<th class="${subject}"><fmt:message key="meta.subject" /></th>
							<td class="${subject}"><input type="text" name="subject" class="textInput" placeholder="subjects value separated by commas"> <img class="delete" alt="delete"
								src="${deleteURL}"></td>
						</tr>
					</c:otherwise>
				</c:choose>

				<c:if test="${metaDisplay['catego'] && weblab:hasValue(resource, rdfProperties['catego'])}">
					<tr>
						<th><fmt:message key="meta.catego" /></th>
						<td><input type="text" name="catego" class="textInput"
							value="<weblab:getPropertyValues
								uri="${rdfProperties['catego'] }" var="categories" /> <c:forEach
								var="catego" items="${categories}" varStatus="status">
								<c:if test="${status.count != 1}">, </c:if>${catego }
							</c:forEach>"></td>
					</tr>
				</c:if>
				<c:choose>
					<c:when test="${metaDisplay['rate'] && weblab:hasValue(resource, rdfProperties['rate'])}">
						<th class="${rate}"><fmt:message key="meta.rate" /></th>
						<td class="${rate}"><c:set var="starSelected">
								<weblab:getPropertyValues uri="${rdfProperties['rate'] }" />
							</c:set>
							<div id="rating" class="rating">
								<c:if test="${starSelected != 5}">
									<c:forEach begin="0" end="${5-starSelected-1}" varStatus="count">
										<span id="star${5-count.index}">&#9734</span>
									</c:forEach>
								</c:if>
								<c:forEach begin="0" end="${starSelected-1}" varStatus="count">
									<span id="star${starSelected-count.index}" class="colorStar">&#9733</span>
								</c:forEach>
							</div> <input type="hidden" name="rate" value="${starSelected}"> <img class="delete" alt="delete" src="${deleteURL}"></td>
					</c:when>
					<c:otherwise>
						<tr class="extraRow">
							<th class="${rate}"><fmt:message key="meta.rate" /></th>
							<td class="${rate}">
								<div id="rating" class="rating">
									<span id="star5">&#9734</span> <span id="star4">&#9734</span> <span id="star3">&#9734</span> <span id="star2">&#9734</span> <span id="star1">&#9734</span>
								</div> <input type="hidden" name="rate"> <img class="delete" alt="delete" src="${deleteURL}">
							</td>
						</tr>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${metaDisplay['comment'] && weblab:hasValue(resource, rdfProperties['comment'])}">
						<tr>
							<th class="${commented}"><fmt:message key="meta.commented" /></th>
							<td class="${commented}"><input type="text" name="comment" value="<weblab:getPropertyValues
								uri="${rdfProperties['comment']}" />" class="textInput">
								<img class="delete" alt="delete" src="${deleteURL}"></td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr class="extraRow">
							<th class="${commented}"><fmt:message key="meta.commented" /></th>
							<td class="${commented}"><input type="text" name="comment" class="textInput" placeholder="Insert your comment"> <img class="delete" alt="delete"
								src="${deleteURL}"></td>
						</tr>
					</c:otherwise>
				</c:choose>

				<c:choose>
					<c:when test="${metaDisplay['credibility'] && weblab:hasValue(resource, rdfProperties['credibility'])}">
						<tr>
							<c:set var="alphabet" value="ABCDEF" />
							<th class="${credibility}"><fmt:message key="meta.credibility" /></th>
							<td class="${credibility}"><c:set var="credibSelected">
									<weblab:getPropertyValues uri="${rdfProperties['credibility']}" />
								</c:set> <select id="credible" onchange="changeSelectValue(this)">
									<option></option>
									<c:forEach var="i" begin="0" end="${fn:length(alphabet)-1}" step="1">
										<c:choose>
											<c:when test="${ credibSelected eq alphabet.charAt(i).toString() }">
												<option value="${alphabet.charAt(i)}" selected>${alphabet.charAt(i)}</option>
											</c:when>
											<c:otherwise>
												<option value="${alphabet.charAt(i)}">${alphabet.charAt(i)}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
							</select> <input type="hidden" name="credibility" value="${credibSelected}"> <img class="delete" alt="delete" src="${deleteURL}"></td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr class="extraRow">
							<c:set var="alphabet" value="ABCDEF" />
							<th class="${credibility}"><fmt:message key="meta.credibility" /></th>
							<td class="${credibility}"><select id="credible" onchange="changeSelectValue(this)">
									<option></option>
									<c:forEach var="i" begin="0" end="${fn:length(alphabet)-1}" step="1">
										<option value="${alphabet.charAt(i)}">${alphabet.charAt(i)}</option>
									</c:forEach>
							</select> <input type="hidden" name="credibility"> <img class="delete" alt="delete" src="${deleteURL}"></td>
						</tr>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${metaDisplay['reliability'] && weblab:hasValue(resource, rdfProperties['reliability'])}">
						<tr>
							<c:set var="numbers" value="123456" />
							<th class="${reliability}"><fmt:message key="meta.reliability" /></th>
							<td class="${reliability}"><c:set var="reliabSelected">
									<weblab:getPropertyValues uri="${rdfProperties['reliability'] }" />
								</c:set> <select id="reliable" onchange="changeSelectValue(this)">
									<option></option>
									<c:forEach var="i" begin="0" end="${fn:length(numbers)-1}" step="1">
										<c:choose>
											<c:when test="${ reliabSelected == numbers.charAt(i).toString() }">
												<option value="${numbers.charAt(i)}" selected>${numbers.charAt(i)}</option>
											</c:when>
											<c:otherwise>
												<option value="${numbers.charAt(i)}">${numbers.charAt(i)}</option>
											</c:otherwise>
										</c:choose>
									</c:forEach>
							</select> <input type="hidden" name="reliability" value="${reliabSelected}"> <img class="delete" alt="delete" src="${deleteURL}"></td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr class="extraRow">
							<c:set var="numbers" value="123456" />
							<th class="${reliability}"><fmt:message key="meta.reliability" /></th>
							<td class="${reliability}"><select id="reliable" onchange="changeSelectValue(this)">
									<option selected></option>
									<c:forEach var="i" begin="0" end="${fn:length(numbers)-1}" step="1">
										<option value="${numbers.charAt(i)}">${numbers.charAt(i)}</option>
									</c:forEach>
							</select> <input type="hidden" name="reliability"> <img class="delete" alt="delete" src="${deleteURL}"></td>
						</tr>
					</c:otherwise>
				</c:choose>
				<%-- 				<c:choose> --%>
				<%-- 					<c:when --%>
				<%-- 						test="${metaDisplay['subjects'] && weblab:hasValue(resource, rdfProperties['subjects'])}"> --%>
				<!-- 						<tr> -->
				<%-- 							<th class="${subjects}"><fmt:message key="meta.subjects" /></th> --%>
				<%-- 							<td class="${subjects}"><input type="text" name="subjects" --%>
				<%-- 								value="<weblab:getPropertyValues --%>
				<%-- 							uri="${rdfProperties['subjects'] }" />" --%>
				<!-- 								class="textInput" placeholder="Insert your keywords here"> <img class="delete" alt="delete" -->
				<%-- 								src="${deleteURL}"></td> --%>
				<!-- 						</tr> -->
				<%-- 					</c:when> --%>
				<%-- 					<c:otherwise> --%>
				<!-- 						<tr class="extraRow"> -->
				<%-- 							<th class="${subjects}"><fmt:message key="meta.subjects" /></th> --%>
				<%-- 							<td class="${subjects}"><input type="text" --%>
				<%-- 								name="${subjects}" class="textInput" --%>
				<!-- 								placeholder="Insert your keywords here"> <img -->
				<%-- 								class="delete" alt="delete" src="${deleteURL}"></td> --%>
				<!-- 						</tr> -->
				<%-- 					</c:otherwise> --%>
				<%-- 				</c:choose> --%>
				<c:choose>
					<c:when test="${metaDisplay['toKeep'] && weblab:hasValue(resource, rdfProperties['toKeep'])}">
						<tr>
							<th class="${toKeep}"><fmt:message key="meta.toKeep" /></th>
							<c:set var="optionSelect">
								<weblab:getPropertyValues uri="${rdfProperties['toKeep'] }" />
							</c:set>
							<td class="${toKeep}"><select onchange="changeSelectValue(this)">
									<option>None</option>
									<c:choose>
										<c:when test="${optionSelect == true }">
											<option value="true" selected>true</option>
											<option value="false">false</option>
										</c:when>
										<c:otherwise>
											<option value="true">true</option>
											<option value="false" selected>false</option>
										</c:otherwise>
									</c:choose>
							</select> <input type="hidden" name="toKeep" value="${optionSelect}"> <img class="delete" alt="delete" src="${deleteURL}"></td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr class="extraRow">
							<th class="${toKeep}"><fmt:message key="meta.toKeep" /></th>
							<td class="${toKeep}"><select onchange="changeSelectValue(this)">
									<option value="None" selected>None</option>
									<option value="true">True</option>
									<option value="false">False</option>
							</select> <input type="hidden" name="toKeep"> <img class="delete" alt="delete" src="${deleteURL}"></td>
						</tr>
					</c:otherwise>
				</c:choose>
				<c:choose>
					<c:when test="${metaDisplay['lat'] && weblab:hasValue(resource, rdfProperties['lat'])}">
						<tr>
							<th class="${lat}"><fmt:message key="meta.location" /></th>
							<td class="${lat}">
								<div class="locationDiv">
									<label for="latEdit"> Lat : </label> <input id="latEdit" type="text" name="${lat}" value="<weblab:getPropertyValues
									uri="${rdfProperties['lat'] }" />"
										style="width: 100px;"> <label for="longEdit"> Lon : </label> <input id="longEdit" type="text" name="${lon}"
										value="<weblab:getPropertyValues
									uri="${rdfProperties['long'] }" />" style="width: 100px;"> <img class="delete" alt="delete" src="${deleteURL}">
								</div>

								<div class="mapContener">
									<div id="mapEdit"></div>
								</div>

							</td>
						</tr>
					</c:when>
					<c:otherwise>
						<tr class="extraRow">
							<th class="${lat}"><fmt:message key="meta.location" /></th>
							<td class="${lat}">
								<div class="locationDiv">
									<label for="latEdit"> Lat : </label> <input id="latEdit" type="text" name="${lat}" style="width: 100px;" placeholder="click on the map"> <label for="longEdit">
										Lon : </label> <input id="longEdit" type="text" name="${lon}" style="width: 100px;" placeholder="click on the map"> <img class="delete" alt="delete" src="${deleteURL}">
								</div>
								<div class="mapContener">
									<div id="mapEdit"></div>
								</div>
							</td>
						</tr>
					</c:otherwise>
				</c:choose>
			</tbody>
		</table>

		<div class="addMetaDiv">
			<select id="addMeta">
				<option value="None" selected="selected"></option>
				<option value="${rate}">${rate}</option>
				<option value="${commented}">${commented}</option>
				<option value="${credibility}">${credibility}</option>
				<option value="${reliability}">${reliability}</option>
				<option value="${subject}">${subject}</option>
				<option value="${toKeep}">${toKeepLabel}</option>
				<option value="${lat}">${docLocation}</option>
			</select>
			<input id="addMetaButton" class="addButton" type="button" value="<fmt:message key="meta.addMetaButton" />">
		</div>

		<div class="actionButton">
			<input id="cancelButton" type="button" value="<fmt:message key="meta.cancelButton" />">
			<input id="saveButton" type="submit" value="<fmt:message key="meta.saveButton" />">
		</div>

	</form>

</div>

<script type="text/javascript">


jQuery(document).ready(
		function() {

			openLayerMap(${useGeoserverExposedMap}, '${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}', ${layersConfig});

			jQuery(".extraRow, .error").hide();

			//init datepicker with all meta data field date.
			jQuery('#created, #modified').datetimepicker({
				//dateFormat : "dollar{timeConfig.formatDateForDateTimePicker}",
				maxDate : new Date(${timeConfig.maxEndDateMS})
			});

			//Save any modification in the current resource.
			jQuery("#saveButton").click(function(e) {
				jQuery("#inputForm").submit();
				e.preventDefault();
			});

			// A function to show new meta data in table

			jQuery("#addMetaButton").click(
					function(e) {
						var metaToDisplay = jQuery("#addMeta option:selected").val();
						console.log(" value of the metaToDisplay : " + metaToDisplay);
						jQuery("." + metaToDisplay).parents("tr").show();
					});

			jQuery(".delete").click(
					function(e) {
						var metaToHide = jQuery(this).parent().attr('class');
						jQuery(this).siblings('input').attr('value', '');
						console.log("metaToHidee : " + metaToHide);
						jQuery(this).parents("tr").hide();
					});

			// submit fake form to go back to view mode
			jQuery("#cancelButton").click(function() {
			    var $form = $('<form>', {
			        action: '${cancelURL}',
			        method: 'post'
			    });
			    $form.appendTo('body').submit();
			});
			
			//Data rating
			jQuery('#star5, #star4, #star3, #star2, #star1').click(function (e){
				$(this).attr("class","colorStar").text("\u2605").nextAll().attr("class","colorStar").text("\u2605");
				$(this).prevAll().text("\u2606").removeClass();
				var myValue = $(this).attr('id').substr(-1);
				$(this).parent().siblings('input').val(myValue);
			});

		});
		
function changeSelectValue(myVal){
	jQuery(myVal).siblings('input').val(myVal.value);
}

function validateForm(theForm){
	jQuery('.error').hide();
	if ((typeof jQuery('#title').val() != 'undefined')){
		if (!jQuery('#title').val()){
			jQuery('.error').show();
			return false;
		}
	}
}
</script>