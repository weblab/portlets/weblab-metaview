<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>


<portlet:defineObjects />


<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="meta_portlet" />

<portlet:resourceURL var="resourceURL" escapeXml="false" id="showMetadata" />
<portlet:resourceURL var="clearErrorURL" escapeXml="false" id="clearErrorAttribute" />

<portlet:actionURL var="useAsSource">
	<portlet:param name="action" value="useAsSource" />
</portlet:actionURL>

<c:choose>
	<c:when test="${isEmptyModel}">
		<div class="portlet-msg-info"><fmt:message key="portlet.no_meta" /></div>
	</c:when>
	<c:otherwise>

		<!-- icons definition -->
		<c:url value="/images/" var="baseURL"></c:url>
		
		<c:choose>
			<c:when test="${editError == true}">
				<div id="saveError" class="portlet-msg-error"><fmt:message key="meta.errorSave" /></div>
			</c:when>
		</c:choose>
		

		<!-- AJAX resource collection -->
		<div id="meta_portlet_meta_view">
			<div id="result_portlet_loading_image">
				<img alt="loading" src="${baseURL}loading_animation.gif">
			</div>
		</div>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery("#meta_portlet_meta_view").load("${resourceURL}");
				var saveError = jQuery('#saveError');
				if (saveError.length){
					saveError.delay(2000).hide("slow",function() {
						jQuery.get('${clearErrorURL}');
					});
				}
			});
		</script>
	</c:otherwise>
</c:choose>

<script type="text/javascript">

	jQuery(document).ready(function() {
	
		var messages = {
			"addButton" : "<fmt:message key='portlet.sources.button.add' />",
			"cancelButton" : "<fmt:message key='portlet.sources.button.cancel' />",
			"useAsSourceContent" : "<fmt:message key='portlet.sources.useAsSourceContent' />",
			"useAsSourceTitle" : "<fmt:message key='portlet.sources.useAsSourceTitle' />",
		};
	
		var urls = {
			"useAsSourceURL" : '${useAsSource}',
		};
		
		jQuery.metaview.init(urls, messages);
	});
	
	
</script>