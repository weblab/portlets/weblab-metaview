<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://org.ow2.weblab/taglib" prefix="weblab"%>


<portlet:defineObjects />

<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="meta_portlet" />
<portlet:resourceURL var="editURL" escapeXml="false" id="editmeta" />

<c:url value="${baseURL }/../weblab-theme/images/icons/manage-16x16.png" var="useAsSourceButtonImgSrc"></c:url>

<!-- alert user when leaving weblab -->
<script type="text/javascript">
	// Leave WebLab
	jQuery(this).on('.warnUserOnClick', "click", function() {
		jQuery(this).target = "_blank";
		var answer = window.confirm('<fmt:message key="portlet.leave" />');
		if (answer) {
			window.open($(this).prop('href'));
		}
		return false;
	});
	jQuery(document).ready(function() {
		openLayerMap(${useGeoserverExposedMap}, '${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}', ${layersConfig});
		jQuery("#buttonAdd").click(function($) {
			jQuery("#editView").slideDown(function() {
				jQuery(".meta_data_table").hide();
				jQuery("#editView").load("${editURL}");
			});
		});
	});
</script>

<!-- variables -->
<c:set var="hasTraduction" value="${metaDisplay['translation'] && !empty(translations)}" />

<!-- meta-data -->
<div>
	<table class="meta_data_table">
		<tbody>
			<c:if test="${metaDisplay['title'] && weblab:hasValue(resource, rdfProperties['title'])}">
				<tr>
					<th><fmt:message key="meta.title" /></th>
					<td><b><i><weblab:getPropertyValues uri="${rdfProperties['title']}" /></i></b></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['source'] && weblab:hasValue(resource, rdfProperties['source'])}">
				<tr>
					<th><fmt:message key="meta.source" /></th>
					<td>
						<c:set var="source">
							<weblab:getPropertyValues uri="${rdfProperties['source'] }" />
						</c:set>
						<c:if test="${fn:startsWith(source, 'http:') || fn:startsWith(source, 'https:')}">
							<c:if test="${userHasAdminRole && allowSourceEventing}">
								<span style="cursor: pointer;" onclick="jQuery.metaview.useAsSource()"><img title="<fmt:message key="meta.useAsSource" />" src="${useAsSourceButtonImgSrc}" /></span>
							</c:if>
						</c:if>
						<c:choose>
							<c:when test="${offline}">
								<c:out value="${source}" />
							</c:when>
							<c:otherwise>
								<weblab:renderValues style="anchor" cssClass="meta_link warnUserOnClick">${source}</weblab:renderValues>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['mainsource'] && weblab:hasValue(resource, rdfProperties['mainsource'])}">
				<tr>
					<th><fmt:message key="meta.mainsource" /></th>
					<td>
						<c:set var="mainsource">
							<weblab:getPropertyValues uri="${rdfProperties['mainsource'] }" />
						</c:set>
						<c:choose>
							<c:when test="${offline}">
								<c:out value="${mainsource}" />
							</c:when>
							<c:otherwise>
								<weblab:renderValues style="anchor" cssClass="meta_link warnUserOnClick">${mainsource}</weblab:renderValues>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['format'] && weblab:hasValue(resource, rdfProperties['format'])}">
				<tr>
					<th><fmt:message key="meta.format" /></th>
					<td>
						<c:choose>
							<c:when test="${metaDisplay['isExposedAs'] && weblab:hasValue(resource, rdfProperties['isExposedAs'])}">
								<c:set var="nativeContentPath">
									${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}<weblab:getPropertyValues uri="${rdfProperties['isExposedAs'] }" />
								</c:set>
								<c:choose>
									<c:when test="${metaDisplay['hasOriginalFileName'] && weblab:hasValue(resource, rdfProperties['hasOriginalFileName'])}">
										<c:set var="hasOriginalFileNameString">
											<weblab:getPropertyValues uri="${rdfProperties['hasOriginalFileName'] }" />
										</c:set>
										<i><a href="${nativeContentPath}" download="${hasOriginalFileNameString}"><weblab:renderValues style="icon+text" cssClass="meta_data_formatIconText"><weblab:getPropertyValues uri="${rdfProperties['format'] }" /></weblab:renderValues></a></i>
									</c:when>
									<c:otherwise>
										<i><a href="${nativeContentPath}" download><weblab:renderValues style="icon+text" cssClass="meta_data_formatIconText"><weblab:getPropertyValues uri="${rdfProperties['format'] }" /></weblab:renderValues></a></i>
									</c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								<weblab:renderValues style="icon+text" cssClass="meta_data_formatIconText"><weblab:getPropertyValues uri="${rdfProperties['format'] }" /></weblab:renderValues>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['language'] && weblab:hasValue(resource, rdfProperties['language'])}">
				<tr>
					<th><fmt:message key="meta.language" /></th>
					<td>
						<c:choose>
							<c:when test="${hasTraduction}">
								<portlet:actionURL var="selectOriginalVersion">
									<portlet:param name="action" value="selectOriginalVersion" />
								</portlet:actionURL>
								<a href="${selectOriginalVersion }"><weblab:renderValues style="icon+text" cssClass="meta_data_formatIconText"><weblab:getPropertyValues uri="${rdfProperties['language'] }" /></weblab:renderValues></a> (<fmt:message key="meta.originalLanguageVersion" />)
							</c:when>
							<c:otherwise>
								<weblab:renderValues style="icon+text" cssClass="meta_data_formatIconText"><weblab:getPropertyValues uri="${rdfProperties['language'] }" /></weblab:renderValues>
							</c:otherwise>
						</c:choose>
					</td>
				</tr>
			</c:if>
			<c:if test="${hasTraduction}">
				<tr>
					<th><fmt:message key="meta.availableTranslations" /></th>
					<td><i>
						<c:forEach var="translation" items="${translations }" varStatus="status">
							<c:if test="${status.count != 1}">
								<br />
							</c:if>
							<portlet:actionURL var="selectTranslation">
								<portlet:param name="action" value="selectTranslation" />
								<portlet:param name="language" value="${translation.key }" />
								<portlet:param name="service" value="${translation.value.uri}" />
							</portlet:actionURL>
							<a href="${selectTranslation }"><weblab:renderValues style="icon+text" cssClass="meta_data_formatIconText">${translation.key }</weblab:renderValues></a> (${translation.value.label })
						</c:forEach>
					</i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['created'] && weblab:hasValue(resource, rdfProperties['created'])}">
				<weblab:getPropertyValues uri="${rdfProperties['created'] }" var="createdDates" />
				<c:set var="createdDatesStr" value="" />
				<c:forEach items="${createdDates}" var="createdDate" varStatus="createdDateStatus">
					<c:if test="${createdDateStatus.count != 1}">
						<c:set var="createdDatesStr" value="${createdDatesStr}<br />" />
					</c:if>
					<c:catch var="createdDatesError">
						<fmt:formatDate value="${createdDate}" type="BOTH" dateStyle="FULL" var="createdDateStr" />
						<c:set var="createdDatesStr" value="${createdDatesStr}${createdDateStr}" />
					</c:catch>
				</c:forEach>
				<c:if test="${createdDatesError == null}">
					<tr>
						<th><fmt:message key="meta.created" /></th>
						<td><i> <c:out value="${createdDatesStr}" />
						</i></td>
					</tr>
				</c:if>
			</c:if>
			<c:if test="${metaDisplay['modified'] && weblab:hasValue(resource, rdfProperties['modified'])}">
				<weblab:getPropertyValues uri="${rdfProperties['modified'] }" var="modifiedDates" />
				<c:set var="modifiedDatesStr" value="" />
				<c:forEach items="${modifiedDates}" var="modifiedDate" varStatus="modifiedDateStatus">
					<c:if test="${modifiedDateStatus.count != 1}">
						<c:set var="modifiedDatesStr" value="${modifiedDatesStr} ; " />
					</c:if>
					<c:catch var="modifiedDatesError">
						<fmt:formatDate value="${modifiedDate}" type="BOTH" dateStyle="FULL" var="modifiedDateStr" />
						<c:set var="modifiedDatesStr" value="${modifiedDatesStr}${modifiedDateStr}" />
					</c:catch>
				</c:forEach>
				<c:if test="${modifiedDatesError == null}">
					<tr>
						<th><fmt:message key="meta.modified" /></th>
						<td><i> <c:out value="${modifiedDatesStr}" />
						</i></td>
					</tr>
				</c:if>
			</c:if>
			<c:if test="${metaDisplay['gatheringDate'] && weblab:hasValue(resource, rdfProperties['gatheringDate'])}">
				<weblab:getPropertyValues uri="${rdfProperties['gatheringDate'] }" var="gatheringDates" />
				<c:set var="gatheringDatesStr" value="" />
				<c:forEach items="${gatheringDates}" var="gatheringDate" varStatus="gatheringDateStatus">
					<c:if test="${gatheringDateStatus.count != 1}">
						<c:set var="gatheringDatesStr" value="${gatheringDatesStr}<br />" />
					</c:if>
					<c:catch var="gatheringDatesError">
						<fmt:formatDate value="${gatheringDate}" type="BOTH" dateStyle="FULL" var="gatheringDateStr" />
						<c:set var="gatheringDatesStr" value="${gatheringDatesStr}${gatheringDateStr}" />
					</c:catch>
				</c:forEach>
				<c:if test="${gatheringDatesError == null}">
					<tr>
						<th><fmt:message key="meta.gatheringDate" /></th>
						<td><i> <c:out value="${gatheringDatesStr}" />
						</i></td>
					</tr>
				</c:if>
			</c:if>
			<c:if test="${metaDisplay['creator'] && weblab:hasValue(resource, rdfProperties['creator'])}">
				<tr>
					<th><fmt:message key="meta.creator" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['creator'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['contributor'] && weblab:hasValue(resource, rdfProperties['contributor'])}">
				<tr>
					<th><fmt:message key="meta.contributor" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['contributor'] }" var="contributors" />
						<c:forEach var="contributor" items="${contributors}" varStatus="status">
							<c:if test="${status.count != 1}">,</c:if>
							${contributor}
						</c:forEach>
					</i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['date'] && weblab:hasValue(resource, rdfProperties['date'])}">
				<weblab:getPropertyValues uri="${rdfProperties['date'] }" var="dates" />
				<c:set var="datesStr" value="" />
				<c:forEach items="${dates}" var="date" varStatus="dateStatus">
					<c:if test="${dateStatus.count != 1}">
						<c:set var="datesStr" value="${datesStr}<br />" />
					</c:if>
					<c:catch var="datesError">
						<fmt:formatDate value="${date}" type="BOTH" dateStyle="FULL" var="dateStr" />
						<c:set var="datesStr" value="${datesStr}${dateStr}" />
					</c:catch>
				</c:forEach>
				<c:if test="${datesError == null}">
					<tr>
						<th><fmt:message key="meta.date" /></th>
						<td><i> <c:out value="${datesStr}" />
						</i></td>
					</tr>
				</c:if>
			</c:if>
			<c:if test="${metaDisplay['identifier'] && weblab:hasValue(resource, rdfProperties['identifier'])}">
				<tr>
					<th><fmt:message key="meta.identifier" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['identifier'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['hasScore'] && weblab:hasValue(resource, rdfProperties['hasScore'])}">
				<tr>
					<th><fmt:message key="meta.hasScore" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['hasScore'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['hasNativeContent'] && weblab:hasValue(resource, rdfProperties['hasNativeContent'])}">
				<tr>
					<th><fmt:message key="meta.hasNativeContent" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['hasNativeContent'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['hasNormalisedContent'] && weblab:hasValue(resource, rdfProperties['hasNormalisedContent'])}">
				<tr>
					<th><fmt:message key="meta.hasNormalisedContent" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['hasNormalisedContent'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['isGeneratedFrom'] && weblab:hasValue(resource, rdfProperties['isGeneratedFrom'])}">
				<tr>
					<th><fmt:message key="meta.isGeneratedFrom" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['isGeneratedFrom'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['abstract'] && weblab:hasValue(resource, rdfProperties['abstract'])}">
				<tr>
					<th><fmt:message key="meta.abstract" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['abstract'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['isProducedFrom'] && weblab:hasValue(resource, rdfProperties['isProducedFrom'])}">
				<tr>
					<th><fmt:message key="meta.isProducedFrom" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['isProducedFrom'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['description'] && weblab:hasValue(resource, rdfProperties['description'])}">
				<tr>
					<th><fmt:message key="meta.description" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['description'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['subject'] && weblab:hasValue(resource, rdfProperties['subject'])}">
				<tr>
					<th><fmt:message key="meta.subject" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['subject'] }" var="subjects" />
						<c:forEach var="subject" items="${subjects }" varStatus="status">
							<c:if test="${status.count != 1}">,</c:if>
							${subject }
						</c:forEach>
					</i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['hasSpeakerLabel'] && weblab:hasValue(resource, rdfProperties['hasSpeakerLabel'])}">
				<tr>
					<th><fmt:message key="meta.speaker" /></th>
					<td><i> <weblab:getPropertyValues uri="${rdfProperties['hasSpeakerLabel'] }" var="speakerLabel" />
						<c:forEach var="spkLabel" items="${speakerLabel }" varStatus="status">
							<c:if test="${status.count != 1}">,</c:if>
							${spkLabel }
						</c:forEach>
					</i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['catego'] && weblab:hasValue(resource, rdfProperties['catego'])}">
				<tr>
					<th><fmt:message key="meta.catego" /></th>
					<td><i> <weblab:getPropertyValues uri="${rdfProperties['catego'] }" var="categories" />
						<c:forEach var="catego" items="${categories}" varStatus="status">
							<c:if test="${status.count != 1}">,</c:if>
							${catego }
						</c:forEach>
					</i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['type'] && weblab:hasValue(resource, rdfProperties['type'])}">
				<tr>
					<th><fmt:message key="meta.type" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['type'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['coverage'] && weblab:hasValue(resource, rdfProperties['coverage'])}">
				<tr>
					<th><fmt:message key="meta.coverage" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['coverage'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['projectName'] && weblab:hasValue(resource, rdfProperties['projectName'])}">
				<tr>
					<th><fmt:message key="meta.projectName" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['projectName'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['hasOriginalFileSize'] && weblab:hasValue(resource, rdfProperties['hasOriginalFileSize'])}">
				<tr>
					<th><fmt:message key="meta.hasOriginalFileSize" /></th>
					<td><weblab:getPropertyValues uri="${rdfProperties['hasOriginalFileSize'] }" var="fileSizes" /> <weblab:renderValues style="fileSize" value="${fileSizes}" /></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['refersTo'] && weblab:hasValue(resource, rdfProperties['refersTo'])}">
				<tr>
					<th><fmt:message key="meta.refersTo" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['refersTo'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['seeAlso'] && weblab:hasValue(resource, rdfProperties['seeAlso'])}">
				<tr>
					<th><fmt:message key="meta.seeAlso" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['seeAlso'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['rate'] && weblab:hasValue(resource, rdfProperties['rate'])}">
				<tr>
					<th><fmt:message key="meta.rate" /></th>
					<td><c:set var="starSelected">
							<weblab:getPropertyValues uri="${rdfProperties['rate'] }" />
						</c:set>
						<div class="displayStar">
							<c:if test="${starSelected != 5}">
								<c:forEach begin="0" end="${5-starSelected-1}" varStatus="count">
									<span id="star${5-count.index}">&#9734</span>
								</c:forEach>
							</c:if>
							<c:forEach begin="0" end="${starSelected-1}">
								<span class="colorStar">&#9733</span>
							</c:forEach>
						</div></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['comment'] && weblab:hasValue(resource, rdfProperties['comment'])}">
				<tr>
					<th><fmt:message key="meta.commented" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['comment'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['credibility'] && weblab:hasValue(resource, rdfProperties['credibility'])}">
				<tr>
					<th><fmt:message key="meta.credibility" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['credibility'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['reliability'] && weblab:hasValue(resource, rdfProperties['reliability'])}">
				<tr>
					<th><fmt:message key="meta.reliability" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['reliability'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['subjects'] && weblab:hasValue(resource, rdfProperties['subjects'])}">
				<tr>
					<th><fmt:message key="meta.subjects" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['subjects'] }" /> </i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['toKeep'] && weblab:hasValue(resource, rdfProperties['toKeep'])}">
				<tr>
					<th><fmt:message key="meta.toKeep" /></th>
					<td><i><weblab:getPropertyValues uri="${rdfProperties['toKeep'] }" /></i></td>
				</tr>
			</c:if>
			<c:if test="${metaDisplay['lat'] && weblab:hasValue(resource, rdfProperties['lat'])}">
				<input id="lat" type="hidden" value="<weblab:getPropertyValues uri="${rdfProperties['lat'] }" />">
				<input id="long" type="hidden" value="<weblab:getPropertyValues uri="${rdfProperties['long'] }" />">
				<tr>
					<th><fmt:message key="meta.location" /></th>
					<td>
						<div class="mapContener">
							<div id="displayMap"></div>
						</div>
					</td>
				</tr>
			</c:if>

		</tbody>
	</table>
</div>

<c:if test="${userHasAdminRole}">
	<div id="editView">
		<input id="buttonAdd" class="right-button addButton" type="button" value="<fmt:message key="meta.editButton"/>">
	</div>
</c:if>


