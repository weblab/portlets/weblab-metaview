function openLayerMap(useGeoserverExposedMap, serverUrlPrefix, layersConfig) {

	console.log(layersConfig.projection);
	if (jQuery('#longEdit').length) {
		var htmlLon = jQuery('#longEdit');
		var htmlLat = jQuery('#latEdit');
		var canvas = jQuery('#mapEdit')[0];
	} else {
		var htmlLon = jQuery('#long');
		var htmlLat = jQuery('#lat');
		var canvas = jQuery('#displayMap')[0];
	}
	var lon = parseFloat(htmlLon.val());
	var lat = parseFloat(htmlLat.val());
	var iconGeometry;
	var centerMap = [ 0, 0 ];

	var iconFeature = new ol.Feature();

	if (!isNaN(lon) && !isNaN(lat)) {
		centerMap = convertToMap([lon, lat], layersConfig.projection);
		setCoordMarker(centerMap);
	}

	var iconStyle = new ol.style.Style({
		image : new ol.style.Circle({
			fill : new ol.style.Fill({
				color : [ 255, 0, 0, 1 ]
			}),
			radius : 5
		})
	});

	iconFeature.setStyle(iconStyle);

	var vectorLayer = new ol.layer.Vector({
		source : new ol.source.Vector({
			features : [ iconFeature ]
		})
	});

	var layers = [];
	if (useGeoserverExposedMap) {	
		for (var i = 0; i < layersConfig.layers.length; i++) {
			var layer = layersConfig.layers[i];

			if (layer.hasOwnProperty("relativeUrl")) {
				layer.url = serverUrlPrefix + layer.relativeUrl;
			}

			var rasterLayer = new ol.layer.Tile({
				source : new ol.source.TileWMS({
					url : layer.url,
					params : {
						'LAYERS' : layer.layer,
						'TILED' : layer.tiled,
						'VERSION' : layer.version,
					},
					serverType : 'geoserver'
				})
			});
			layers.push(rasterLayer);
		}
	} else {
		rasterLayer = new ol.layer.Tile({
			source : new ol.source.OSM()
		});
		layers.push(rasterLayer);
	}

	jQuery('.mapContener').css('width', '100%');

	var view = new ol.View({
		center : centerMap,
		zoom : 3,
		projection: layersConfig.projection
	});

	layers.push(vectorLayer);

	var map = new ol.Map({
		layers : layers,
		target : canvas,
		view : view,
		controls : ol.control.defaults({
			zoom : false,
			attribution : false,
			rotate : false
		})
	});

	map.on('singleclick', function(evt) {
		if (jQuery('#longEdit').length) {
			var coord = evt.coordinate;
			setCoordMarker(coord);
			console.log(coord);
			setLonLatInput(convertToLonLat(coord, layersConfig.projection));
		}
	});

	htmlLon.keyup(keyUpEvt);

	htmlLat.keyup(keyUpEvt);

	function keyUpEvt() {
		if (htmlLon.val() == '') {
			htmlLon.val(0);
		}
		if (htmlLat.val() == '') {
			htmlLat.val(0);
		}
		lon = parseFloat(htmlLon.val());
		lat = parseFloat(htmlLat.val());
		if (!isNaN(lon) && !isNaN(lat)) {
			console.log(layersConfig.projection);
			setCoordMarker(convertToMap([lon, lat], layersConfig.projection));
		}
	}
	htmlLon.change(changeEvt);

	htmlLat.change(changeEvt);

	function changeEvt() {
		// Remove useless zeros
		htmlLon.val(lon.toString());
		htmlLat.val(lat.toString());
	}

	function setLonLatInput(lonlat) {
		htmlLon.val(lonlat[0]);
		htmlLat.val(lonlat[1]);
	}

	function setCoordMarker(coord) {
		if (!iconGeometry) {
			iconGeometry = new ol.geom.Point(coord);
			iconFeature.setGeometry(iconGeometry);
		}
		iconGeometry.setCoordinates(coord);
	}

	function convertToMap(lonlat, projection) {
		return ol.proj.transform(lonlat, 'EPSG:4326', projection);
	}

	function convertToLonLat(coord, projection) {
		return (projection =='EPSG:4326') ? coord : ol.proj.transform(coord, projection, 'EPSG:4326');
	}

	function centerToGeolocation(position) {
		view.setCenter(convertToMap([ position.coords.longitude, position.coords.latitude ]));
	}
}