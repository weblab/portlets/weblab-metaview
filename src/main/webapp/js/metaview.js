jQuery.metaview = {

	init : function(urls, messages) {

		jQuery.metaview.urls = urls;
		jQuery.metaview.messages = messages;
	},

	useAsSource : function() {

		var useAsSourceDialog = jQuery('<div>'
				+ jQuery.metaview.messages.useAsSourceContent + '</div>');

		var buttonAdd = {
			text : jQuery.metaview.messages.addButton,
			click : function() {
				document.location.href = jQuery.metaview.urls.useAsSourceURL;
			}
		}

		var buttonCancel = {
			text : jQuery.metaview.messages.cancelButton,
			click : function() {
				useAsSourceDialog.dialog("close");
			}
		}

		useAsSourceDialog.dialog({
			modal : true,
			width : 500,
			title : jQuery.metaview.messages.useAsSourceTitle,
			buttons : [ buttonCancel, buttonAdd ]
		});
	}
}