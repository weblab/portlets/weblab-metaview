/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2019 AIRBUS Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.mvc.spring.controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.portlet.business.bean.EditMetaConfBean;
import org.ow2.weblab.portlet.business.bean.TimeConfig;
import org.ow2.weblab.portlet.business.bean.config.map.GroupLayerConfig;
import org.ow2.weblab.portlet.business.bean.taglib.DataTaglib;
import org.ow2.weblab.portlet.business.service.PermissionService;
import org.ow2.weblab.portlet.business.service.ResourceSaverService;
import org.ow2.weblab.portlet.business.service.impl.EditMetaServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The controller in charge of the edition.
 *
 * @author kdesir, ymombrun
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "resource", "isEmptyModel", "isLoadedResource", "rdfProperties", "translations", "metaDisplay", "editMetaData", "timeConfig", "dataTaglib", "dataLanguage",
		"useGeoserverExposedMap", "layersConfig", "editError" })
public class EditMetaController {


	private static final String USE_GEOSERVER_EXPOSED_MAP = "useGeoserverExposedMap";


	private static final String URL_REDIRECTION = "/web/guest/docview";


	private static final String LAYERS_CONFIG = "layersConfig";


	private static final String EDIT_ERROR = "editError";


	private static final String RESOURCE = "resource";


	private static final String TIME_CONFIG = "timeConfig";


	private static final String EDIT_META_DATA = "editMetaData";


	private static final String DATA_LANGUAGE = "dataLanguage";


	private static final String DATA_TAGLIB = "dataTaglib";


	private static final String UTF_8 = "UTF-8";


	private static final String TEXT_HTML = "text/html";


	private final Log logger = LogFactory.getLog(this.getClass());


	private final EditMetaConfBean editMetaData;


	private final DataTaglib dataTaglib;


	private final TimeConfig timeConfig;


	private final ResourceSaverService resourceSaverService;


	private final PermissionService permissionService;


	public EditMetaController(final EditMetaConfBean editMetaData, final TimeConfig timeConfig, final DataTaglib dataTaglib, final ResourceSaverService resourceSaverService,
			final PermissionService permissionService) {

		super();
		this.editMetaData = editMetaData;
		this.timeConfig = timeConfig;
		this.dataTaglib = dataTaglib;
		this.resourceSaverService = resourceSaverService;
		this.permissionService = permissionService;
	}


	@ModelAttribute("layersConfig")
	public GroupLayerConfig getLayersConfig() {

		return this.editMetaData.getLayersConfig();
	}


	@ResourceMapping("editmeta")
	public ModelAndView displayEditMenu(final ResourceRequest request, final ResourceResponse response) {

		response.setContentType(EditMetaController.TEXT_HTML);
		response.setCharacterEncoding(EditMetaController.UTF_8);
		this.logger.debug("ShowView EditMetaController called");
		final ModelAndView mav = new ModelAndView();
		final ModelMap model = (ModelMap) request.getPortletSession().getAttribute("model");
		if (!model.containsAttribute(EditMetaController.TIME_CONFIG)) {
			model.addAttribute(EditMetaController.TIME_CONFIG, this.timeConfig);
		}
		if (!model.containsAttribute(EditMetaController.DATA_TAGLIB) || !model.containsAttribute(EditMetaController.DATA_LANGUAGE)) {
			model.addAttribute(EditMetaController.DATA_TAGLIB, this.dataTaglib.getFormats());
			model.addAttribute(EditMetaController.DATA_LANGUAGE, this.dataTaglib.getLanguages());
		}

		if (!model.containsAttribute(EditMetaController.EDIT_META_DATA)) {
			model.addAttribute(EditMetaController.EDIT_META_DATA, this.editMetaData.getProperties());
		}

		if (!model.containsAttribute(EditMetaController.USE_GEOSERVER_EXPOSED_MAP)) {
			model.addAttribute(EditMetaController.USE_GEOSERVER_EXPOSED_MAP, Boolean.valueOf(this.editMetaData.isUseGeoserverExposedMap()));
		}

		if (!model.containsAttribute(EditMetaController.EDIT_ERROR)) {
			model.addAttribute(EditMetaController.EDIT_ERROR, Boolean.FALSE);
		}
		mav.setView("editview");
		mav.addAllObjects(model);
		return mav;
	}


	@ResourceMapping("clearErrorAttribute")
	public void clearModelAttribute(final ModelMap model) {

		model.addAttribute(EditMetaController.EDIT_ERROR, Boolean.FALSE);
	}


	@ActionMapping(params = "action=cancel")
	public void cancelEdit(final ActionRequest actionRequest, final ActionResponse response, final ModelMap model) throws IOException {

		this.logger.debug("view redirect after cancel method called");
		response.sendRedirect(EditMetaController.URL_REDIRECTION);
	}


	@ActionMapping(params = "action=save")
	public void saveEdit(final ActionRequest request, final ActionResponse response, final ModelMap model) throws SystemException, IOException {

		this.logger.debug("saveEdit method called");

		if (!this.permissionService.hasAdminRole(request)) {
			this.logger.error("The current user does not have the role to be authorized to edit the metadata of any document!");
			return;
		}

		if (this.resourceSaverService == null) {
			this.logger.error("No resource saver service configured !!!");
			return;
		}

		final Resource resource = (Resource) model.get(EditMetaController.RESOURCE);
		final Map<String, List<String>> editMeta = (LinkedHashMap<String, List<String>>) model.get(EditMetaController.EDIT_META_DATA);
		final Map<String, String[]> parameterMap = request.getParameterMap();
		final EditMetaServices editMetaServices = new EditMetaServices(editMeta, this.timeConfig);
		editMetaServices.writeContent(resource, parameterMap);
		try {
			final boolean worked = this.resourceSaverService.saveResource(resource);
			if (!worked) {
				this.logger.warn("The resource " + resource.getUri() + " may not be saved.");
			}
			model.addAttribute(EditMetaController.EDIT_ERROR, Boolean.valueOf(!worked));
		} catch (final IOException | WebLabCheckedException e) {
			model.addAttribute(EditMetaController.EDIT_ERROR, Boolean.TRUE);
			this.logger.error("An error occured saving the resource " + resource.getUri() + ".", e);
		}
		model.addAttribute(EditMetaController.RESOURCE, resource);
		request.getPortletSession().setAttribute("model", model);
		response.sendRedirect(EditMetaController.URL_REDIRECTION);
	}
}
