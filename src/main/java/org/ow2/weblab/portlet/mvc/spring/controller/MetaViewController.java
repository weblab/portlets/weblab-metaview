/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.mvc.spring.controller;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.xml.namespace.QName;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.portlet.business.bean.DisplayMetaConfBean;
import org.ow2.weblab.portlet.business.bean.RDFPropertiesConfBean;
import org.ow2.weblab.portlet.business.bean.config.map.GroupLayerConfig;
import org.ow2.weblab.portlet.business.service.MetaViewBusinessServices;
import org.ow2.weblab.portlet.business.service.PermissionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * Metaview portlet Spring MVC controller.
 * Business layer is accessible by Spring business services
 *
 * Control actions, renders, resources and events requests.
 *
 * @author emilienbondu
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "resource", "isEmptyModel", "isLoadedResource", "rdfProperties", "translations", "metaDisplay", "editMetaData", "timeConfig", "dataTaglib", "dataLanguage",
		"useGeoserverExposedMap", "layersConfig", "editError", "offline" })
public class MetaViewController {


	private static final String RESOURCE_DESC = "resourceDesc";


	private static final String VIEW = "view";


	/**
	 * Key for the resource field.
	 */
	protected static final String RESOURCE = "resource";


	private static final String IS_LOADED_RESOURCE = "isLoadedResource";


	private static final String IS_EMPTY_MODEL = "isEmptyModel";


	private static final String HAS_ADMIN_ROLE = "userHasAdminRole";


	protected static final String ALLOW_SOURCE_EVENTING = "allowSourceEventing";


	private static final String OFFLINE = "offline";


	// attributes
	protected final Log logger = LogFactory.getLog(this.getClass());


	protected final MetaViewBusinessServices metaviewBusinessService;


	protected final PermissionService permissionService;


	protected final DisplayMetaConfBean metaDisplayConf;


	protected final RDFPropertiesConfBean rdfProperties;


	protected final String sourceTypeTab;


	protected final Boolean allowSourceEventing;


	protected final Boolean offline;



	public MetaViewController(final MetaViewBusinessServices metaviewBusinessService, final PermissionService permissionService, final DisplayMetaConfBean metaDisplayConf,
			final RDFPropertiesConfBean rdfProperties, final String sourceTypeTab, final Boolean allowSourceEventing, final Boolean offline) {

		super();
		this.metaviewBusinessService = metaviewBusinessService;
		this.permissionService = permissionService;
		this.metaDisplayConf = metaDisplayConf;
		this.rdfProperties = rdfProperties;
		this.sourceTypeTab = sourceTypeTab;
		this.allowSourceEventing = allowSourceEventing;
		this.offline = offline;
	}


	@ModelAttribute("metaDisplay")
	public Map<String, Boolean> getDisplayMeta() {

		return this.metaDisplayConf.getProperties();
	}


	@ModelAttribute("useGeoserverExposedMap")
	public Boolean getUseGeoserverExposedMap() {

		return Boolean.valueOf(this.metaDisplayConf.isUseGeoserverExposedMap());
	}


	@ModelAttribute("layersConfig")
	public GroupLayerConfig getLayersConfig() {

		return this.metaDisplayConf.getLayersConfig();
	}


	@ModelAttribute("rdfProperties")
	public Map<String, String> getRDFProperties() {

		return this.rdfProperties.getProperties();
	}


	// #############################################################################
	// action mapping methods #
	// #############################################################################
	@ActionMapping(params = "action=goToEntity")
	public void goToEntity(final ActionResponse response, final ModelMap model) {

		this.logger.debug("receiving action goToEntity");
		response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/metaview/action}sendUnloadedDocument"), (PieceOfKnowledge) model.get(MetaViewController.RESOURCE));
	}


	@ActionMapping(params = "action=selectTranslation")
	public void selectTranslation(final ActionRequest request, final ActionResponse response, final ModelMap model) {

		final String lang = request.getParameter("language");
		final URI service = URI.create(request.getParameter("service"));
		this.logger.debug("select translation for language :" + lang + " and service" + service);
		response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/metaview/action}selectTranslatedVersion"),
				this.metaviewBusinessService.getPoKForSelectedTranslation(URI.create(((Resource) model.get(MetaViewController.RESOURCE)).getUri()), service, lang));
	}


	@ActionMapping(params = "action=selectOriginalVersion")
	public void selectOriginal(final ActionRequest request, final ActionResponse response, final ModelMap model) {

		this.logger.debug("select original version");
		response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/metaview/action}selectOriginalVersion"), ((Resource) model.get(MetaViewController.RESOURCE)).getUri());
	}


	@ActionMapping(params = "action=useAsSource")
	public void useAsSource(final ActionRequest request, final ActionResponse response, final ModelMap model) throws SystemException {

		this.logger.info("send event useAsSource");

		if (!this.permissionService.hasAdminRole(request)) {
			this.logger.warn("The current user does not have the role to be authorized to use this as a source!");
			return;
		}

		if ((this.allowSourceEventing == null) || !this.allowSourceEventing.booleanValue()) {
			this.logger.warn("The source eventing is deactivated by configuration!");
			return;
		}

		final Document doc = (Document) model.get(MetaViewController.RESOURCE);
		final PieceOfKnowledge pok = this.metaviewBusinessService.getPoKToUseAsSource(doc, this.sourceTypeTab);
		response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/metaview/action}useAsSource"), pok);
	}


	/**
	 * The user add the current document to the basket. An event to share this action is sent to others Portlets.
	 *
	 * @param response
	 *            The action response
	 * @param model
	 *            The model
	 */
	@ActionMapping(params = "action=addToBasketMeta")
	public void addToBasket(final ActionResponse response, final ModelMap model) {

		response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/metaview/action}addToBasket"), (Document) model.get(MetaViewController.RESOURCE));
	}


	/**
	 * The user pin the current document to the basket. An event to share this action is sent to others Portlets.
	 *
	 * @param response
	 *            The action response
	 * @param model
	 *            The model
	 */
	@ActionMapping(params = "action=pinToBasketMeta")
	public void pinToBasket(final ActionResponse response, final ModelMap model) {

		response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/metaview/action}pinToBasket"), (Document) model.get(MetaViewController.RESOURCE));
	}


	// #############################################################################
	// event mapping methods #
	// #############################################################################
	@EventMapping(value = "{http://weblab.ow2.org/portlet/metaview/reaction}displayMeta")
	public void processLoadedDocumentEvent(final EventRequest request, final EventResponse response, final ModelMap model) {

		// receive a event containing a document
		this.logger.debug("receiving the event: {http://weblab.ow2.org/portlet/metaview/reaction}displayMeta with value " + request.getEvent());

		model.addAttribute(MetaViewController.RESOURCE, request.getEvent().getValue());
		model.addAttribute(MetaViewController.RESOURCE_DESC, new HashMap<String, List<String>>());
		model.addAttribute(MetaViewController.IS_LOADED_RESOURCE, Boolean.FALSE);
		model.addAttribute(MetaViewController.IS_EMPTY_MODEL, Boolean.FALSE);
	}


	@EventMapping(value = "{http://weblab.ow2.org/portlet/metaview/reaction}loadDocumentAndDisplayMeta")
	public void processNotLoadedDocumentEvent(final EventRequest request, final EventResponse response, final ModelMap model) {

		// receive a event containing an unloaded document
		this.logger.debug("receiving the event : {http://weblab.ow2.org/portlet/metaview/reaction}loadDocumentAndDisplayMeta with value " + request.getEvent());

		model.addAttribute(MetaViewController.RESOURCE, request.getEvent().getValue());
		model.addAttribute(MetaViewController.RESOURCE_DESC, new HashMap<String, List<String>>());
		model.addAttribute(MetaViewController.IS_LOADED_RESOURCE, Boolean.FALSE);
		model.addAttribute(MetaViewController.IS_EMPTY_MODEL, Boolean.FALSE);

		// response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/metaview/action}sendUnloadedDocument"), (PieceOfKnowledge)model.get("resource"));
	}


	// #############################################################################
	// resource mapping methods #
	// #############################################################################
	@ResourceMapping("showMetadata")
	public ModelAndView serveMetadata(final ResourceRequest request, final ResourceResponse response, final ModelMap model)
			throws ContentNotAvailableException, InvalidParameterException, UnexpectedException, SystemException {

		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		final ModelAndView mav = new ModelAndView();
		mav.setView("view_meta_ajax");
		mav.addAllObjects(model);
		final Resource resource = (Resource) model.get(MetaViewController.RESOURCE);
		this.logger.debug("serving the metadata content table from resource: " + resource.getUri());

		// loading map if needed
		boolean isLoadedDocument = false;
		if (model.containsAttribute(MetaViewController.IS_LOADED_RESOURCE)) {
			isLoadedDocument = ((Boolean) model.get(MetaViewController.IS_LOADED_RESOURCE)).booleanValue();
		}

		// allow events to be send to source portlet
		mav.getModelMap().addAttribute(MetaViewController.ALLOW_SOURCE_EVENTING, this.allowSourceEventing);

		// check admin permissions for user
		mav.getModelMap().addAttribute(MetaViewController.HAS_ADMIN_ROLE, Boolean.valueOf(this.permissionService.hasAdminRole(request)));

		mav.getModelMap().addAttribute(MetaViewController.OFFLINE, this.offline);

		// adding map to model
		if (!isLoadedDocument) {
			// loading resource from pok by calling service
			mav.getModelMap().addAttribute(MetaViewController.RESOURCE, this.metaviewBusinessService.getWLResource(this.metaviewBusinessService.getURIFromPoK((PieceOfKnowledge) resource)));
			mav.getModelMap().addAttribute(MetaViewController.IS_LOADED_RESOURCE, Boolean.TRUE);
			mav.getModelMap().addAttribute("translations", this.metaviewBusinessService.getTranslations((Resource) mav.getModelMap().get(MetaViewController.RESOURCE), request.getLocale()));
		}
		request.getPortletSession().setAttribute("model", mav.getModelMap());
		return mav;
	}


	// #############################################################################
	// render mapping methods #
	// #############################################################################
	@RenderMapping()
	public ModelAndView showMetadata(final ModelMap model) {

		final ModelAndView mav = new ModelAndView(MetaViewController.VIEW);
		mav.addAllObjects(model);
		if (!model.containsAttribute(MetaViewController.IS_EMPTY_MODEL)) {
			this.logger.debug("Is empty model.");
			mav.addObject(MetaViewController.IS_EMPTY_MODEL, Boolean.TRUE);
		}
		return mav;
	}


	// #############################################################################
	// exceptions handlers #
	// #############################################################################
	@ExceptionHandler(Exception.class)
	public ModelAndView getMetadataException(final Exception ex) {

		final ModelAndView mav = new ModelAndView("error");
		mav.addObject("message_error", ex.getMessage());
		return mav;
	}

}
