/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.bean.config.map;

import com.google.gson.GsonBuilder;


/**
 * Configuration holder for a layer used within OpenLayers. This class is abstract and has to be extended to represent special types of layers.
 */
public abstract class LayerConfig implements Cloneable {



	private Double minResolution, maxResolution;


	private Double opacity;


	private Boolean visible;


	private final String type;


	private String nameKey;


	/**
	 * @return the nameKey
	 */
	public String getNameKey() {
		return this.nameKey;
	}


	/**
	 * @param nameKey
	 *            the nameKey to set
	 */
	public void setNameKey(final String nameKey) {
		this.nameKey = nameKey;
	}


	/**
	 * Default constructor that sets the type as simple class name value.
	 */
	public LayerConfig() {
		this.type = this.getClass().getSimpleName();
	}


	/**
	 * @return the minResolution
	 */
	public Double getMinResolution() {
		return this.minResolution;
	}


	/**
	 * @param minResolution
	 *            the minResolution to set
	 */
	public void setMinResolution(final Double minResolution) {
		this.minResolution = minResolution;
	}


	/**
	 * @return the maxResolution
	 */
	public Double getMaxResolution() {
		return this.maxResolution;
	}


	/**
	 * @param maxResolution
	 *            the maxResolution to set
	 */
	public void setMaxResolution(final Double maxResolution) {
		this.maxResolution = maxResolution;
	}


	/**
	 * @return the opacity
	 */
	public Double getOpacity() {
		return this.opacity;
	}


	/**
	 * @param opacity
	 *            the opacity to set
	 */
	public void setOpacity(final Double opacity) {
		this.opacity = opacity;
	}


	/**
	 * @return the visible
	 */
	public Boolean isVisible() {
		return this.visible;
	}


	/**
	 * @return the visible
	 */
	public Boolean getVisible() {
		return this.isVisible();
	}


	/**
	 * @param visible
	 *            the visible to set
	 */
	public void setVisible(final Boolean visible) {
		this.visible = visible;
	}


	/**
	 * @return the type
	 */
	public String getType() {
		return this.type;
	}


	/**
	 * Copies the values of field of <code>this</code> to the <code>clone</code>.
	 * Only copies the values that are in <code>LayerConfig</code>.
	 *
	 * @param clone
	 *            The clone whose properties has to be set with the value of this
	 */
	protected void enrichClone(final LayerConfig clone) {
		clone.setMaxResolution(this.getMaxResolution());
		clone.setMinResolution(this.getMinResolution());
		clone.setOpacity(this.getOpacity());
		clone.setVisible(this.getVisible());
		clone.setNameKey(this.getNameKey());
	}


	@Override
	protected abstract Object clone();


	@Override
	public String toString() {
		return new GsonBuilder().create().toJson(this);
	}

}
