/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.service.impl;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.annotator.AbstractAnnotator.Operator;
import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.portlet.business.bean.TimeConfig;
import org.purl.dc.elements.DublinCoreAnnotator;

/**
 * A class to edit meta data contained in resources.
 *
 * @author kdesir
 *
 */

public class EditMetaServices {


	private final Map<String, List<String>> editMetaConf;


	private final TimeConfig timeConfig;


	private final Log logger = LogFactory.getLog(this.getClass());


	public EditMetaServices(final Map<String, List<String>> editMeta, final TimeConfig timeConfig) {
		this.editMetaConf = editMeta;
		this.timeConfig = timeConfig;
	}


	public Map<String, String[]> getParameterMap(final ActionRequest request) {
		return request.getParameterMap();
	}


	/**
	 * Modify the resource according field that the user have filled.
	 *
	 * @param resource
	 *            the current resource to modify
	 * @param parameterMap
	 *            a map which represents the key-value parameters of the edit
	 *            form
	 * @return
	 * 		the modified resource.
	 */
	public Resource writeContent(final Resource resource, final Map<String, String[]> parameterMap) {
		for (final String keyParam : parameterMap.keySet()) {
			if (this.editMetaConf.containsKey(keyParam)) {
				final List<String> valueEditConf = this.editMetaConf.get(keyParam);
				try {
					this.writeWithType(resource, valueEditConf, keyParam, parameterMap);
				} catch (final ParseException e) {
					this.logger.error("Unable to parse date value, the correct format must be MM/dd/yyyy HH:mm", e);
				}
			}
		}
		return resource;
	}


	/**
	 * @param resource
	 *            the current resource to modify.
	 * @param valueEditConf
	 *            the list of value linked with the meta data field such as
	 *            "title" field will have : [dc , http://namespace , string ].
	 * @param keyParam
	 *            the actual field that will be modified.
	 * @param parameterMap
	 *            a map which represents the key-value parameters of the edit
	 *            form.
	 * @throws ParseException
	 *             if the date is not in the right format.
	 */
	private void writeWithType(final Resource resource, final List<String> valueEditConf, final String keyParam, final Map<String, String[]> parameterMap) throws ParseException {
		final String value = parameterMap.get(keyParam)[0];
		final String xlmns = valueEditConf.get(0);
		final String entireNameSpace = valueEditConf.get(1);
		final String valueType = valueEditConf.get(2);

		// use the right method according to the key Parameter we have.
		if (!value.isEmpty()) {
			if (valueType.equals("string")) {
				this.write(resource, xlmns, URI.create(entireNameSpace), keyParam, String.class, value);
			} else if (valueType.equals("long")) {
				this.write(resource, xlmns, URI.create(entireNameSpace), keyParam, Long.class, Long.valueOf(value));
			} else if (valueType.equals("double")) {
				this.write(resource, xlmns, URI.create(entireNameSpace), keyParam, Double.class, Double.valueOf(value));
			} else if (valueType.equals("date")) {
				final SimpleDateFormat dateFormat = new SimpleDateFormat(this.timeConfig.getFormatDateTimeForJSTL());
				this.write(resource, xlmns, URI.create(entireNameSpace), keyParam, Date.class, dateFormat.parse(value.toString()));
			} else if (valueType.equals("integer")) {
				this.write(resource, xlmns, URI.create(entireNameSpace), keyParam, Integer.class, Integer.valueOf(value));
			}
		}
		// if empty, erase the current field of the resource.
		else {
			this.write(resource, xlmns, URI.create(entireNameSpace), keyParam, null, null);
		}

	}


	public Map<String, List<String>> getEditMetaConf() {
		return this.editMetaConf;
	}


	/**
	 *
	 * @param resource
	 *            the current resource to modify.
	 * @param prefix
	 *            the prefix
	 * @param propertyURI
	 *            the namespace of the property.
	 * @param localname
	 *            the localname of the property.
	 * @param clazz
	 *            the class of the value
	 * @param val
	 *            the current value
	 * @param <T>
	 *            The provided type of the value to write
	 */
	public <T> void write(final Resource resource, final String prefix, final URI propertyURI, final String localname, final Class<T> clazz, final T val) {
		final BaseAnnotator ba = new BaseAnnotator(resource);
		if (localname.equals("subject") && (val != null)) {
			final DublinCoreAnnotator dc = new DublinCoreAnnotator(resource);
			final String[] mySubject = ((String) val).split(",");
			ba.setAppendMode(false);
			ba.applyOperator(Operator.WRITE, prefix, propertyURI, localname, clazz, null);
			for (final String value : mySubject) {
				dc.writeSubject(value.trim());
			}
		} else {
			ba.setAppendMode(false);
			ba.applyOperator(Operator.WRITE, prefix, propertyURI, localname, clazz, null);
			ba.applyOperator(Operator.WRITE, prefix, propertyURI, localname, clazz, val);
		}
	}

}
