/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.service;

import java.net.URI;
import java.util.Locale;
import java.util.Map;

import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.portlet.business.bean.ServiceDescription;

public interface MetaViewBusinessServices {


	public Resource getWLResource(final String docURI) throws ContentNotAvailableException, InvalidParameterException, UnexpectedException;


	public String getURIFromPoK(final PieceOfKnowledge pok);


	public Map<String, ServiceDescription> getTranslations(final Resource res, final Locale prefered);


	public PieceOfKnowledge getPoKForSelectedTranslation(final URI uri, final URI serviceURI, final String lang);


	public PieceOfKnowledge getPoKToUseAsSource(final Document doc, final String sourceTab);

}
