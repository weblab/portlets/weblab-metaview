/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.bean;

/**
 * A service description bean.
 *
 * @author emilienbondu
 */
public class ServiceDescription {


	private String uri, label;


	/**
	 * @return the uri
	 */
	public String getUri() {

		return this.uri;
	}


	/**
	 * @param uri
	 *            the uri to set
	 */
	public void setUri(final String uri) {

		this.uri = uri;
	}


	/**
	 * @return the label
	 */
	public String getLabel() {

		return this.label;
	}


	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(final String label) {

		this.label = label;
	}

}
