/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.service;

import java.io.IOException;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.model.Resource;

/**
 * Interface defining the service is charge of saving the resource back
 * 
 * @author ymombrun
 */
public interface ResourceSaverService {


	/**
	 * @param resource
	 *            The resource to be saved
	 * @return Whether or not we are sure that the resource has been saved. <code>false</code> can be used when we are not sure
	 * @throws IOException
	 *             If an error occurred saving the resource
	 * @throws WebLabCheckedException
	 *             If the resource cannot be serialised as XML
	 */
	public boolean saveResource(final Resource resource) throws IOException, WebLabCheckedException;

}
