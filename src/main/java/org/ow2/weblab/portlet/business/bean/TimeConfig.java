/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.bean;

import java.util.Date;

import javax.annotation.Resource;

/**
 * @author kdesir
 */
@Resource
public class TimeConfig {


	private long maxEndDateMS;


	private String formatDateTimeForJSTL;


	/**
	 * The default constructor that initialises todays date
	 */
	public TimeConfig() {

		this.setMaxEndDateMS(new Date());
	}


	/**
	 * @return the max end date in ms
	 */
	public long getMaxEndDateMS() {

		return this.maxEndDateMS;
	}


	/**
	 * @param maxEndDateMS
	 *            The max end date (using a date object)
	 */
	public void setMaxEndDateMS(final Date maxEndDateMS) {

		this.maxEndDateMS = maxEndDateMS.getTime();
	}


	/**
	 * @return the formatDateTimeForJSTL
	 */
	public String getFormatDateTimeForJSTL() {

		return this.formatDateTimeForJSTL;
	}


	/**
	 * @param formatDateTimeForJSTL
	 *            the formatDateTimeForJSTL to set
	 */
	public void setFormatDateTimeForJSTL(final String formatDateTimeForJSTL) {

		this.formatDateTimeForJSTL = formatDateTimeForJSTL;
	}

}
