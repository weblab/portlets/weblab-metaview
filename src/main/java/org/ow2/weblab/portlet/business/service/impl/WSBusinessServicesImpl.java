/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.service.impl;

import java.net.URI;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.helper.impl.SemanticResource;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.ResourceContainer;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.resourcecontainer.LoadResourceArgs;
import org.ow2.weblab.portlet.business.bean.ServiceDescription;
import org.ow2.weblab.portlet.business.service.MetaViewBusinessServices;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.springframework.stereotype.Service;

import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.QuerySolutionMap;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.sparql.resultset.ResultSetMem;

@Service
public class WSBusinessServicesImpl implements MetaViewBusinessServices {


	private static final String SERVICE = "service";


	private static final String LABEL = "label";


	private static final String LANG = "lang";


	protected final Log logger = LogFactory.getLog(this.getClass());


	protected final ResourceContainer service;


	private final String linkedToQuery;


	private final String translationsQueryTemplate;


	/**
	 * Standard constructor of the class, setting the resource container and some parameters
	 *
	 * @param service
	 *            The the resource container client
	 * @param linkedToQuery
	 *            The query that search the linked resource URI inside the received pok
	 * @param translationsQueryTemplate
	 *            The query template to be used to search for available translations inside the loaded document
	 * @throws WebLabCheckedException
	 *             If the resource container cannot be loaded
	 */
	public WSBusinessServicesImpl(final ResourceContainer service, final String linkedToQuery, final String translationsQueryTemplate) throws WebLabCheckedException {
		this.service = service;
		this.linkedToQuery = linkedToQuery;
		this.translationsQueryTemplate = translationsQueryTemplate;
	}



	@Override
	public Resource getWLResource(final String docURI) throws ContentNotAvailableException, InvalidParameterException, UnexpectedException {
		final LoadResourceArgs args = new LoadResourceArgs();
		args.setResourceId(docURI);
		try {
			return this.service.loadResource(args).getResource();
		} catch (final Exception e) {
			final String message = "Caught an error when calling Resource container service to retrieve document " + docURI + ".";
			this.logger.error(message, e);
			throw ExceptionFactory.createUnexpectedException(message, e);
		}
	}


	/**
	 * Method to get URI from a receive PieceOfKnowledge using POK_PROPERTY_DOC_URI
	 *
	 * @param pok
	 *            The event received, containing a uri of document to load
	 * @return URI if exist null otherwise
	 */
	@Override
	public String getURIFromPoK(final PieceOfKnowledge pok) {
		if (pok != null) {
			final SemanticResource semRes = new SemanticResource(pok);
			final ResultSetMem rsm = semRes.selectAsJenaResultSet(this.linkedToQuery);
			if (rsm.hasNext()) {
				final com.hp.hpl.jena.rdf.model.Resource resource = rsm.next().getResource("resource");
				if ((resource != null) && resource.isURIResource()) {
					return resource.getURI();
				}
			}
			this.logger.error("Received pok (" + pok.getUri() + ") is not linked to a document URI.");
		} else {
			this.logger.error("Received pok was null.");
		}
		return null;
	}


	@Override
	public Map<String, ServiceDescription> getTranslations(final Resource resource, final Locale preferred) {
		final HashMap<String, ServiceDescription> translations = new HashMap<>();

		final QuerySolutionMap qsm = new QuerySolutionMap();
		qsm.add("preferred", ResourceFactory.createPlainLiteral(preferred.getLanguage()));
		qsm.add("resource", ResourceFactory.createResource(resource.getUri()));
		final ParameterizedSparqlString query = new ParameterizedSparqlString(this.translationsQueryTemplate, qsm);

		final SemanticResource sem = new SemanticResource(resource);

		final ResultSetMem rs = sem.selectAsJenaResultSet(query.toString());

		while (rs.hasNext()) {
			final QuerySolution qs = rs.next();

			if (!translations.containsKey(qs.getLiteral(WSBusinessServicesImpl.LANG).toString())) {
				final ServiceDescription serviceDesc = new ServiceDescription();
				serviceDesc.setUri(qs.getResource(WSBusinessServicesImpl.SERVICE).getURI().toString());
				serviceDesc.setLabel(qs.getLiteral(WSBusinessServicesImpl.LABEL).getLexicalForm());
				translations.put(qs.getLiteral(WSBusinessServicesImpl.LANG).toString(), serviceDesc);
				this.logger.debug("Translation found: " + qs.getLiteral(WSBusinessServicesImpl.LANG).toString() + " - " + qs.getLiteral(WSBusinessServicesImpl.LABEL).getLexicalForm());
			}

			if (translations.containsKey(qs.get(WSBusinessServicesImpl.LANG)) && !qs.getLiteral(WSBusinessServicesImpl.LABEL).getLanguage().isEmpty()) {
				final ServiceDescription serviceDesc = new ServiceDescription();
				serviceDesc.setUri(qs.getResource(WSBusinessServicesImpl.SERVICE).getURI().toString());
				serviceDesc.setLabel(qs.getLiteral(WSBusinessServicesImpl.LABEL).getLexicalForm());
				translations.put(qs.getLiteral(WSBusinessServicesImpl.LANG).toString(), serviceDesc);
				this.logger.debug("Translation found: " + qs.getLiteral(WSBusinessServicesImpl.LANG).toString() + " - " + qs.getLiteral(WSBusinessServicesImpl.LABEL).getLexicalForm());
			}
		}

		return translations;
	}


	@Override
	public PieceOfKnowledge getPoKForSelectedTranslation(final URI uri, final URI serviceURI, final String lang) {
		final PieceOfKnowledge pok = new PieceOfKnowledge();
		pok.setUri("weblab://" + UUID.randomUUID());

		final WProcessingAnnotator wpa = new WProcessingAnnotator(uri, pok);
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(uri, pok);
		wpa.writeProducedBy(serviceURI);
		dca.writeLanguage(lang);

		return pok;
	}


	@Override
	public PieceOfKnowledge getPoKToUseAsSource(final Document doc, final String sourceTab) {
		final PieceOfKnowledge pok = new PieceOfKnowledge();
		pok.setUri("weblab://" + UUID.randomUUID());

		final URI uri = URI.create(pok.getUri());

		final DublinCoreAnnotator dcaDoc = new DublinCoreAnnotator(doc);

		final WProcessingAnnotator wpaPok = new WProcessingAnnotator(uri, pok);
		final DublinCoreAnnotator dcaDPok = new DublinCoreAnnotator(uri, pok);

		wpaPok.writeLabel(dcaDoc.readSource().firstTypedValue());
		dcaDPok.writeType(sourceTab);

		return pok;
	}

}
