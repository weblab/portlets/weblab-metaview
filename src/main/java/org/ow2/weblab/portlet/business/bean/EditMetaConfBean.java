/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.bean;

import java.util.List;
import java.util.Map;

import org.ow2.weblab.portlet.business.bean.config.map.GroupLayerConfig;

/**
 * Contains edition configuration
 *
 * @author kdesir
 */
public class EditMetaConfBean {


	private boolean useGeoserverExposedMap;


	private GroupLayerConfig layersConfig;


	private Map<String, List<String>> properties;


	/**
	 * @return The properties
	 */
	public Map<String, List<String>> getProperties() {

		return this.properties;
	}


	/**
	 * @param properties
	 *            The properties to set
	 */
	public void setProperties(final Map<String, List<String>> properties) {

		this.properties = properties;
	}


	/**
	 * @return the useGeoserverExposedMap
	 */
	public boolean isUseGeoserverExposedMap() {

		return this.useGeoserverExposedMap;
	}


	/**
	 * @param useGeoserverExposedMap
	 *            the useGeoserverExposedMap to set
	 */
	public void setUseGeoserverExposedMap(final boolean useGeoserverExposedMap) {

		this.useGeoserverExposedMap = useGeoserverExposedMap;
	}


	/**
	 * @return The group layer config
	 */
	public GroupLayerConfig getLayersConfig() {

		return this.layersConfig;
	}


	/**
	 * @param layersConfig
	 *            The layer config to set
	 */
	public void setLayersConfig(final GroupLayerConfig layersConfig) {

		this.layersConfig = layersConfig;
	}

}
