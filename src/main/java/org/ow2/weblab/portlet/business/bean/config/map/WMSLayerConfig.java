/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2016 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.bean.config.map;


/**
 * The WMS Source/layer configuration handler.
 */
public class WMSLayerConfig extends LayerConfig {


	private static final String DEFAULT_WMS_VERSION = "1.3.0";

	private String url, relativeUrl, layer, version = WMSLayerConfig.DEFAULT_WMS_VERSION;


	private boolean tiled = false;


	/**
	 * @return the tiled
	 */
	public boolean isTiled() {
		return this.tiled;
	}


	/**
	 * @param tiled
	 *            the tiled to set
	 */
	public void setTiled(final boolean tiled) {
		this.tiled = tiled;
	}


	/**
	 * @return the relativeUrl
	 */
	public String getRelativeUrl() {
		return this.relativeUrl;
	}



	/**
	 * @param relativeUrl
	 *            the relativeUrl to set
	 */
	public void setRelativeUrl(final String relativeUrl) {
		this.relativeUrl = relativeUrl;
	}


	/**
	 * @return the url
	 */
	public String getUrl() {
		return this.url;
	}



	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(final String url) {
		this.url = url;
	}


	/**
	 * @return the layer
	 */
	public String getLayer() {
		return this.layer;
	}


	/**
	 * @param layer
	 *            the layer to set
	 */
	public void setLayer(final String layer) {
		this.layer = layer;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return this.version;
	}


	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(final String version) {
		this.version = version;
	}

	@Override
	protected Object clone() {
		final WMSLayerConfig clone = new WMSLayerConfig();
		this.enrichClone(clone);
		clone.setUrl(this.getUrl());
		clone.setRelativeUrl(this.getRelativeUrl());
		clone.setLayer(this.getLayer());
		clone.setTiled(this.isTiled());
		clone.setVersion(this.getVersion());
		return clone;
	}

}
