/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.bean.config.map;

import java.util.LinkedList;
import java.util.List;


/**
 * @author fdangelo
 */
public class GroupLayerConfig extends LayerConfig {


	private static final String EPSG_4326 = "EPSG:4326";


	private List<LayerConfig> layers;


	private String projection = GroupLayerConfig.EPSG_4326;


	/**
	 * @param glc
	 *            The layer config
	 */
	public GroupLayerConfig(final GroupLayerConfig glc) {

		this.layers = new LinkedList<>();
		if (glc != null) {
			for (final LayerConfig layer : glc.layers) {
				this.layers.add((LayerConfig) layer.clone());
			}
			this.projection = glc.projection;
		}
	}


	/**
	 * The default constructor
	 */
	public GroupLayerConfig() {

		this(null);
	}


	/**
	 * @return the layers
	 */
	public List<LayerConfig> getLayers() {

		return this.layers;
	}


	/**
	 * @param layers
	 *            the layers to set
	 */
	public void setLayers(final List<LayerConfig> layers) {

		this.layers = layers;
	}


	/**
	 * @return the projection
	 */
	public String getProjection() {

		return this.projection;
	}


	/**
	 * @param projection
	 *            the projection to set
	 */
	public void setProjection(final String projection) {

		this.projection = projection;
	}



	@Override
	protected Object clone() {

		final GroupLayerConfig clone = new GroupLayerConfig();
		this.enrichClone(clone);
		clone.projection = this.projection;
		if (this.getLayers() != null) {
			final LinkedList<LayerConfig> clonedLayers = new LinkedList<>();
			for (final LayerConfig layer : this.getLayers()) {
				clonedLayers.add((LayerConfig) layer.clone());
			}
			clone.setLayers(clonedLayers);
		}
		return clone;
	}

}
