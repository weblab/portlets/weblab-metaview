/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.service.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProxySelector;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.impl.conn.ProxySelectorRoutePlanner;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.portlet.business.service.ResourceSaverService;
import org.springframework.stereotype.Service;

/**
 * Implementation of {@link ResourceSaverService} that calls a REST endpoint to post the resource as XML.
 *
 * @author ymombrun
 */
@Service
public class RestResourceSaverService implements ResourceSaverService {


	private final URI url;


	private final PoolingClientConnectionManager cm;


	private final HttpParams httpParams;


	private final Log logger;


	private static final ContentType APPLICATION_XML = ContentType.create("application/xml", Consts.UTF_8);


	/**
	 * @param url
	 *            The URL of the rest service to be called in order to save the edition
	 * @throws MalformedURLException
	 *             If the URL is not valid
	 * @throws URISyntaxException
	 *             If the url of the servce cannot be converted into an URI
	 */
	public RestResourceSaverService(final String url) throws MalformedURLException, URISyntaxException {

		super();
		this.logger = LogFactory.getLog(this.getClass());
		this.url = new URL(url).toURI();

		this.cm = new PoolingClientConnectionManager();
		this.cm.setMaxTotal(10);
		this.cm.setDefaultMaxPerRoute(10);

		this.httpParams = new BasicHttpParams();

		HttpConnectionParams.setConnectionTimeout(this.httpParams, 4000);
		HttpConnectionParams.setSoTimeout(this.httpParams, 30000);
		this.logger.debug("RestResourceSaverService created for " + url);
	}



	@Override
	public boolean saveResource(final Resource resource) throws IOException, WebLabCheckedException {

		final DefaultHttpClient httpClient = new DefaultHttpClient(this.cm, this.httpParams);

		if ((System.getProperty("http.proxyHost") != null) || "true".equals(System.getProperty("java.net.useSystemProxies"))) {
			this.logger.debug("Proxy information found in system properties. Use default proxy selector.");
			httpClient.setRoutePlanner(new ProxySelectorRoutePlanner(this.cm.getSchemeRegistry(), ProxySelector.getDefault()));
		} else {
			this.logger.trace("No proxy configured.");
		}

		final HttpPost httpPost = new HttpPost(this.url);
		try {
			final StringEntity xmlEntity = new StringEntity(ResourceUtil.saveToXMLString(resource), RestResourceSaverService.APPLICATION_XML);
			httpPost.setEntity(xmlEntity);

			final HttpResponse response = httpClient.execute(httpPost);

			EntityUtils.consumeQuietly(response.getEntity());

			final int statusCode = response.getStatusLine().getStatusCode();
			if (((statusCode >= 200) && (statusCode < 300)) || (statusCode == 304)) {
				return true;
			}
			this.logger.warn("Received an error HTTP code (" + statusCode + ") when accessing saving resource " + resource.getUri() + ".");

		} finally {
			// Release connection
			httpPost.reset();
		}
		return false;
	}


}
