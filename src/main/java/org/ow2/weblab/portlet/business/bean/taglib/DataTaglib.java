/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space SAS
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.bean.taglib;

import java.util.Map;

/**
 * A class to load data taglib properties such as formats and languages.
 *
 * @author kdesir
 */
public class DataTaglib {


	private Map<String, String> formats;


	private Map<String, String> languages;


	/**
	 * @return the formats
	 */
	public Map<String, String> getFormats() {

		return this.formats;
	}


	/**
	 * @param formats
	 *            the formats to set
	 */
	public void setFormats(final Map<String, String> formats) {

		this.formats = formats;
	}


	/**
	 * @return the languages
	 */
	public Map<String, String> getLanguages() {

		return this.languages;
	}


	/**
	 * @param languages
	 *            the languages to set
	 */
	public void setLanguages(final Map<String, String> languages) {

		this.languages = languages;
	}

}
