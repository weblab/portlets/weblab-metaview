/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.service.impl;

import java.util.List;

import javax.portlet.PortletRequest;

import org.ow2.weblab.portlet.business.service.PermissionService;
import org.springframework.stereotype.Service;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;

/**
 * Implementation of {@link PermissionService} using Liferay role permissions.
 *
 * @author rgauthier
 */
@Service
public class LiferayPermissionServiceImpl implements PermissionService {


	private final List<String> adminRoles;


	public LiferayPermissionServiceImpl(final List<String> adminRoles) {
		this.adminRoles = adminRoles;
	}


	@Override
	public boolean hasAdminRole(final PortletRequest request) throws SystemException {
		final ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		final User user = themeDisplay.getUser();

		for (final String role : this.adminRoles) {
			for (final Role userRole : user.getRoles()) {
				if (userRole.getName().equals(role)) {
					return true;
				}
			}
		}
		return false;
	}

}
