# WebLab Metaview (Metadata Portlet)

This project is a Maven based Java project of a Portlet.
This portlet is able to display the metadata associated to a WebLab Document (mainly Dublin Core, but also specific properties).


# Build status
[![build status](https://gitlab.ow2.org/weblab/portlets/weblab-metaview/badges/develop/build.svg)](https://gitlab.ow2.org/weblab/portlets/weblab-metaview/commits/develop)

# How to build it

In order to be build it, you need to have access to the Maven dependencies it is using. Most of the dependencies are in the central repository and thus does not implies specific configuration.
However, the WebLab Core dependencies are not yet included in the Maven central repository but in a dedicated one that we manage ourselves.
Thus you may have to add the repositories that are listed in the settings.xml. 
